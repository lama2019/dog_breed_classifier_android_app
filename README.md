# DoggoFlow

This android app extends the tensor flow demo app "TensorFlow Lite image classification" (https://github.com/tensorflow/examples/blob/master/lite/examples/image_classification/android/README.md)

The models have been changed to enable it to classify dog breeds.

You can download the app here: https://drive.google.com/file/d/1uxGYBfZePOvaBJzUBv2OHHK-kk4HlZAE/view?usp=sharing